# Maintainer: Heiko Baums <heiko@baums-on-web.de>
# Contributor: Kurt J. Bosch <kjb-temp-2009 at alpenjodel.de>

pkgname=fbsplash-systemd
pkgver=1.5.4.4
pkgrel=2
pkgdesc="A splash screen for Linux, integrated into systemd bootup "
arch=('i686' 'x86_64')
url="http://fbsplash.alanhaggai.org"
license=('GPL')
depends=('miscsplashutils' 'freetype2' 'libjpeg' 'libpng' 'libmng' 'lcms' 'gpm') 
optdepends=('linux-fbcondecor: enable console background images'
'fbsplash-extras: additional functionality like daemon icons'
'uswsusp-fbsplash: suspend to disk with fbsplash'
'python: convert themes from splashy to fbsplash')
conflicts=('fbsplash' 'initscripts-extras-fbsplash')
options=('!makeflags')
provides=('fbsplash')
backup=('etc/conf.d/fbcondecor' 'etc/conf.d/splash')
install=fbsplash.install
source=(http://fbsplash.alanhaggai.org/tarballs/files/splashutils-${pkgver}.tar.bz2
splash_start_initcpio.patch
splash.conf
fbsplash.initcpio_install
fbsplash.initcpio_hook
fbcondecor.daemon
fbsplash.sh
fbsplash.service
fbsplash-shutdown.service
fbsplash-reboot.service
fbcondecor.conf
fbcondecor.service)

build() {
    cd "$srcdir/splashutils-$pkgver"

    autoreconf -i

    # fix fbcondecor_ctl splash type
    sed -e 's,fbsplash_lib_init(fbspl_bootup),fbsplash_lib_init(fbspl_undef),' -i src/fbcon_decor_ctl.c

    # fix libdir
    sed -i "s:/lib/splash/cache:/usr/lib/splash/cache:g" debian/splashutils.dirs
    sed -i "s:/lib/splash/tmp:/usr/lib/splash/tmp:g" debian/splashutils.dirs
    sed -i "s:/lib/splash/cache:/usr/lib/splash/cache:g" debian/splash.conf
    sed -i "s:/lib/splash/cache:/usr/lib/splash/cache:g" debian/changelog
    sed -i "s:/lib/splash/tmp:/usr/lib/splash/tmp:g" debian/changelog
    sed -i "s:/lib/splash/cache:/usr/lib/splash/cache:g" docs/daemon
    sed -i "s:@libdir@/splash/cache:/usr/lib/splash/cache:g" scripts/splash-functions.sh.in
    sed -i "s:@libdir@/splash/tmp:/usr/lib/splash/tmp:g" scripts/splash-functions.sh.in
    sed -i "s:@libdir@/splash/bin:/usr/lib/splash/bin:g" scripts/splash-functions.sh.in
    sed -i "s:/lib/splash/cache:/usr/lib/splash/cache:g" src/daemon_cmd.c
    sed -i "s:@libdir@/splash/sys:/usr/lib/splash/sys:g" scripts/splash_geninitramfs.in
    sed -i "s:@libdir@/splash:/usr/lib/splash:g" scripts/splash_manager.in
    sed -i "s:@libdir@/splash:/usr/lib/splash:g" src/fbsplash.h.in

    # fix set_event_dev call for initcpio usage (if evdev module is there)
    #patch -Np2 -i "$srcdir/splash_start_initcpio.patch"

    export LIBS="-lbz2"
    ./configure --prefix=/usr --sysconfdir=/etc --without-klibc --enable-fbcondecor --with-gpm --with-mng --with-png --with-ttf --with-ttf-kernel
    make
}

package() {
    cd "$srcdir/splashutils-$pkgver"

    make DESTDIR="$pkgdir" install

    cd "$pkgdir"

    # fix duplicate slashes to get splash_cache_cleanup grep to work
    sed -r -e 's,^(export spl_.*="/)/+,\1,' -i sbin/splash-functions.sh

    # fix the path to splash_util
    sed -r -e 's,^(export spl_util=)\"/bin/,\1"/sbin/,' -i sbin/splash-functions.sh

    # provide the mountpoint needed by splash-functions.sh
    install -m755 -d usr/lib/splash/{cache,tmp}
    install -m700 -d usr/lib/splash/sys

    # Install fbsplash scripts and config file
    install -D -m644 "$srcdir/splash.conf" "etc/conf.d/splash"
    install -D -m644 "$srcdir/fbsplash.service" "usr/lib/systemd/system/fbsplash.service"
    install -D -m644 "$srcdir/fbcondecor.service" "usr/lib/systemd/system/fbcondecor.service"
    install -D -m644 "$srcdir/fbsplash-shutdown.service" "usr/lib/systemd/system/fbsplash-shutdown.service"
    install -D -m644 "$srcdir/fbsplash-reboot.service" "usr/lib/systemd/system/fbsplash-reboot.service"
    install -D -m644 "$srcdir/fbsplash.sh" "usr/lib/splash/bin/fbsplash.sh"
    install -D -m644 "$srcdir/fbsplash.initcpio_install" "usr/lib/initcpio/install/fbsplash"
    install -D -m644 "$srcdir/fbsplash.initcpio_hook" "usr/lib/initcpio/hooks/fbsplash"

    # Install fbcodecor script and config file
    install -D -m644 "$srcdir/fbcondecor.conf" "etc/conf.d/fbcondecor"
    install -D -m755 "$srcdir/fbcondecor.daemon" "usr/lib/splash/bin/fbcondecor"
}

md5sums=('2a16704c4adde97b58812cd89e3f2342'
'4045e315c52f5a576fca4f7e634eeb91'
'278cffde0ce105234bb5a6865fb7ad94'
'6d1ed97070acee2f0419369d1db38102'
'fbe7e366661d34d70a872413967d38cc'
'a234d9c4f54a455d930e762c79f50807'
'8f675674b15c8b9fe3379bcaf220b606'
'd44a5e2e04f9fbf9a68844fc2aaf7b66'
'41405360c1f5c1167fe89e0cb74401b7'
'0c97c0a0f727b87c8916969ae594c6b8'
'b3db9d4fd902b62ac9e38589677e2d16'
'7a23d22d580f78c62a66a776bed98fbf')
