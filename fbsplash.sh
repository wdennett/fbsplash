#!/bin/bash
export spl_daemon="/sbin/fbsplashd.static"
export spl_pidfile="${spl_cachedir}/daemon.pid"
export spl_decor="//sbin/fbcondecor_ctl.static"
. /sbin/splash-functions.sh
splash_warn() {
    echo "$*" 
}
splash_comm_send() {
    [[ $( /bin/pidof -o %PPID $spl_daemon ) ]] && echo "$@" >$spl_fifo &
}
splash_get_boot_message() {
    if [ "${RUNLEVEL}" = "6" ]; then
        echo "${SPLASH_REBOOT_MESSAGE}"
    elif [ "${RUNLEVEL}" = "0" ]; then
        echo "${SPLASH_SHUTDOWN_MESSAGE}"
    else
        echo "${SPLASH_BOOT_MESSAGE}"
    fi
}
splash_start() {
    if [ "${SPLASH_MODE_REQ}" = "verbose" ]; then
        ${spl_decor} -c on 2>/dev/null
        return 0
    elif [ "${SPLASH_MODE_REQ}" != "silent" ]; then
        return 0
    fi

    # Display a warning if the system is not configured to display init messages
    # on tty1. This can cause a lot of problems if it's not handled correctly, so
    # we don't allow silent splash to run on incorrectly configured systems.
    if [ "${SPLASH_MODE_REQ}" = "silent" -a "${SPLASH_SANITY}" != "insane" ]; then
        if [ -z "$(grep -E '(^| )CONSOLE=/dev/tty1( |$)' /proc/cmdline)" -a \
            -z "$(grep -E '(^| )console=tty1( |$)' /proc/cmdline)" ]; then
        clear
        splash_warn "You don't appear to have a correct console= setting on your kernel"
        splash_warn "command line. Silent splash will not be enabled. Please add"
        splash_warn "console=tty1 or CONSOLE=/dev/tty1 to your kernel command line"
        splash_warn "to avoid this message."
        if [ -n "$(grep 'CONSOLE=/dev/tty1' /proc/cmdline)" -o \
            -n "$(grep 'console=tty1' /proc/cmdline)" ]; then
        splash_warn "Note that CONSOLE=/dev/tty1 and console=tty1 are general parameters and"
        splash_warn "not splash= settings."
    fi
    return 1
fi

if [ -n "$(grep -E '(^| )CONSOLE=/dev/tty1( |$)' /proc/cmdline)" ]; then
    mount -n --bind / ${spl_tmpdir}
    if [ ! -c "${spl_tmpdir}/dev/tty1" ]; then
        umount -n ${spl_tmpdir}
        splash_warn "The filesystem mounted on / doesn't contain the /dev/tty1 device"
        splash_warn "which is required for the silent splash to function properly."
        splash_warn "Silent splash will not be enabled. Please create the appropriate"
        splash_warn "device node to avoid this message."
        return 1
    fi
    umount -n ${spl_tmpdir}
fi
        fi

        rm -f "${spl_pidfile}"

        # Prepare the communications FIFO
        rm -f "${spl_fifo}" 2>/dev/null
        mkfifo "${spl_fifo}"

        local options=""
        [ "${SPLASH_KDMODE}" = "GRAPHICS" ] && options="--kdgraphics"
        [ -n "${SPLASH_EFFECTS}" ] && options="${options} --effects=${SPLASH_EFFECTS}"
        [ "${SPLASH_TEXTBOX}" = "yes" ] && options="${options} --textbox"

        local ttype="bootup"
        if [ "${RUNLEVEL}" = "6" ]; then
            ttype="reboot"
        elif [ "${RUNLEVEL}" = "0" ]; then
            ttype="shutdown"
        fi

        # Start the splash daemon
        BOOT_MSG="$(splash_get_boot_message)" ${spl_daemon} --theme="${SPLASH_THEME}" --pidfile="${spl_pidfile}" --type=${ttype} ${options}

        # Set the silent TTY and boot message
        splash_comm_send "set tty silent ${SPLASH_TTY}"

        if [ "${SPLASH_MODE_REQ}" = "silent" ]; then
            splash_comm_send "set mode silent"
            splash_comm_send "repaint"
            ${spl_decor} -c on 2>/dev/null
        fi

        splash_comm_send "set autoverbose ${SPLASH_AUTOVERBOSE}"

        splash_set_event_dev

        return 0
    }

    splash_begin() {
        if ! [[ $( /bin/pidof -o %PPID $spl_daemon ) ]]; then
            if [ -x /etc/splash/$SPLASH_THEME/scripts/rc_init-pre ]; then
                /etc/splash/$SPLASH_THEME/scripts/rc_init-pre ${0#/etc/rc.d} $RUNLEVEL
            fi &&
                splash_start 
        fi
    }

    # Stop the splash daemon - if any
    splash_stop() {
        if [[ $( /bin/pidof -o %PPID $spl_daemon ) ]]; then
            SPLASH_PUSH_MESSAGES="no" 
            splash_comm_send progress 63535; splash_comm_send paint; /usr/bin/sleep .5
            splash_comm_send "exit staysilent"
            # Wait for painting/fadeout
            local -i i=0
            while [[ i++ -lt 100 && $( /bin/pidof -o %PPID $spl_daemon ) ]]; do
                /usr/bin/sleep .1
            done
            if [ -e "${spl_pidfile}" ]; then 
                rm -fr "${spl_pidfile}"
            fi
        fi
    }
    if [ "$1" = 'stop' ]; then
        RUNLEVEL="5"
	export spl_cachedir="/run/.splash-cache"
	export spl_fifo="${spl_cachedir}/.splash"
        splash_stop
    elif [ "$1" = 'shutdown' ]; then
        RUNLEVEL="0"
	SPLASH_TTY="tty8"
        SPLASH_MODE_REQ="silent"
	SPLASH_THEME="piano"
        export SPLASH_THEME RUNLEVEL SPLASH_TTY SPLASH_MODE_REQ
	splash_start
    elif [ "$1" = 'reboot' ]; then
        RUNLEVEL="6"
	SPLASH_TTY="tty8"
        SPLASH_MODE_REQ="silent"
	SPLASH_THEME="piano"
        export SPLASH_THEME RUNLEVEL SPLASH_TTY SPLASH_MODE_REQ
	splash_start
    fi
    exit 0

